#ifndef _LED_FUNC_
#define _LED_FUNC_

#include "stdint.h"
#include "stm32f4xx_hal.h"

#define LED_MAX 4

void turnLed(uint32_t turnedLed, uint32_t defLed);
void switchLed(uint32_t ledCount);
#endif /*_LED_FUNC_*/
