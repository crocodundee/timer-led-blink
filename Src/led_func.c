#include "led_func.h"

void turnLed(uint32_t turnedLed, uint32_t defLed)
{
    HAL_GPIO_WritePin(GPIOD, defLed, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOD, turnedLed, GPIO_PIN_SET);
}

void switchLed(uint32_t ledCount)
{
    switch(ledCount)
    {
        case 0:
        {
            turnLed(ONE_Pin, TWO_Pin|TREE_Pin|FOUR_Pin);
            break;
        }
        case 1:
        {
            turnLed(TWO_Pin, ONE_Pin|TREE_Pin|FOUR_Pin);
            break;
        }
        case 2:
        {
            turnLed(TREE_Pin, ONE_Pin|TWO_Pin|FOUR_Pin);
            break;
        }
        case 3:
        {
            turnLed(FOUR_Pin, ONE_Pin|TWO_Pin|TREE_Pin);
            break;
        }
        case 4:
        {
            HAL_GPIO_WritePin(GPIOD, ONE_Pin|TWO_Pin|TREE_Pin|FOUR_Pin, GPIO_PIN_RESET);
            break;
        }
        default:
            break;
    }
}
